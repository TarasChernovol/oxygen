import $ from 'jquery';
import Glide from '@glidejs/glide';

let $body, $window, $document, $header;

$body = $('body');
$window = $(window);
$document = $(document);
$header = $('header');

function assembleGlide() {
    let $glideList = $('.js-glide');
    if ($glideList.length) {
        $glideList.each(function (i, element) {
            let $element, config, glide;
            $element = $(element);

            if ($element.hasClass('js-glide--assembled')) {
                return;
            }

            glide = new Glide(element, {
                type: 'carousel',
                animationDuration: 300,
                gap: 10,
            });

            glide.on('build.after', function () {
                $element.removeClass('js-glide--not-loaded');
            });

            glide.mount();

            $element.addClass('js-glide--assembled');
        });
    }
}

assembleGlide();

// MAIN SCREEN
//

let $mainScreen = $('.js-main-screen');
$mainScreen.height($window.height());
$window.resize(function () {
    $mainScreen.height($window.height());
});

// MOBILE MENU
//

let $mobileMenu, $mobileMenuOpen, $mobileMenuClose;
$mobileMenu = $('.js-mobile-menu');
$mobileMenuOpen = $('.js-mobile-menu__open');
$mobileMenuClose = $('.js-mobile-menu__close');

$mobileMenuOpen.click(function () {
    $mobileMenu.fadeIn(100);
    $body.addClass('no-scroll');
    $mobileMenuOpen.fadeOut(100);
});

function closeMobileMenu() {
    $mobileMenu.fadeOut(100);
    $body.removeClass('no-scroll');
    $mobileMenuOpen.fadeIn(100);
}

$mobileMenuClose.click(closeMobileMenu);

// TABS
//

let tabNav;

tabNav = document.querySelectorAll('ul.tab-nav > li');

for (let i = 0; i < tabNav.length; i++) {
    tabNav[i].addEventListener('click', function (event) {
        event.preventDefault();

        for (let i = 0; i < tabNav.length; i++) {
            tabNav[i].classList.remove('active');
        }

        event.currentTarget.classList.add('active');

        let tabPaneList, anchorReference, activePane;

        tabPaneList = document.querySelectorAll('.tab-pane');
        for (let i = 0; i < tabPaneList.length; i++) {
            tabPaneList[i].classList.remove('active');
        }

        document.querySelector(event.target.getAttribute('href')).classList.add('active');
    })
}

// SCREENS
//

function getPercentOfViewPort($document, $window, $element) {
    let windowHeight = $window.height(),
        docScroll = $document.scrollTop(),
        elementTop = $element.offset().top,
        elementHeight = $element.height(),
        hiddenBefore = docScroll - elementTop,
        hiddenAfter = (elementTop + elementHeight) - (docScroll + windowHeight);

    if ((docScroll > elementTop + elementHeight) || (elementTop > docScroll + windowHeight)) {
        return 0;
    } else {
        let result = 80;

        if (hiddenBefore > 0) {
            result -= (hiddenBefore * 100) / elementHeight;
        }

        if (hiddenAfter > 0) {
            result -= (hiddenAfter * 100) / elementHeight;
        }

        return result;
    }
}

$body.on('click', '.js-menu__item', function (event) {
    let $item, block, offset;
    $item = $(event.target).closest('.js-menu__item');
    block = $item.attr('data-block');
    offset = 110;

    closeMobileMenu();
    $('html, body').animate({scrollTop: $('.js-menu__block[data-block="' + block + '"]').offset().top - offset}, 300);
});

let prevBlock = null;
$window.scroll(function () {
    let maxPercent, percent, $currentTarget, block;

    maxPercent = 0;
    $('.js-menu__block').each((i, element) => {
        let $element = $(element);
        percent = getPercentOfViewPort($document, $window, $element);
        if (percent > maxPercent) {
            maxPercent = percent;
            $currentTarget = $element;
        }
    });

    if ($currentTarget) {
        block = $currentTarget.attr('data-block');
        if (block !== prevBlock) {
            prevBlock = block;
            $('.js-menu__item').removeClass('active');
            $('.js-menu__item[data-block="' + block + '"]').addClass('active');
        }
    }

    if (prevBlock !== null) {
        if (prevBlock !== 'home') {
            $header.addClass('header--narrow');
        } else {
            $header.removeClass('header--narrow');
        }
    }
});
