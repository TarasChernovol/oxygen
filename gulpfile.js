let gulp = require('gulp');

//
// IMAGES
//

let image = require('gulp-image');

gulp.task('bundle_image', bundle_image);

function bundle_image() {
    return gulp.src('assets/images/**/*')
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            gifsicle: true,
            svgo: true,
            concurrent: 10,
            quiet: true // defaults to false
        }))
        .pipe(gulp.dest('public/images'))
}

//
// STYLES
//

let concat = require('gulp-concat');
let postcss = require('gulp-postcss');
let nano = require('cssnano');
let next = require('postcss-cssnext');
let sass = require('gulp-sass')(require('sass'));

/**
 *
 */
gulp.task('bundle_css', bundle_css);

function bundle_css() {
    return gulp.src([
        // css
        'assets/styles/reset.css',
        'node_modules/@glidejs/glide/dist/css/glide.core.min.css',
        'node_modules/@glidejs/glide/dist/css/glide.theme.min.css',
        // scss
        'assets/styles/main.scss',
    ])
        .pipe(concat('main.min.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            next,
            nano,
        ]))
        .pipe(gulp.dest('public/styles'));
}

//
// SCRIPTS
//

let browserify = require('browserify');
let babelify = require('babelify');
let source = require('vinyl-source-stream');
let buffer = require('vinyl-buffer');
let uglify = require('gulp-uglify');

gulp.task('bundle_js', bundle_js);

function bundle_js() {
    return browserify({
        entries: [
            'assets/scripts/main.js',
        ],
        debug: false
    })
        .transform(babelify, {
            presets: [
                'env',
            ],
            plugins: [
                'transform-class-properties',
            ]
        })
        .bundle()
        .pipe(source('main.min.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('public/scripts'));
}

/**
 * SET UP
 */

gulp.task('default', gulp.series(
    bundle_css,
    bundle_js,
    //bundle_image,
));

gulp.task('ww', function () {
    bundle_js();
    bundle_css();

    gulp.watch('assets/**/*.scss', bundle_css);

    gulp.watch([
        'assets/**/*.js'
    ], bundle_js);
});

gulp.task('w', function () {
    bundle_css();
    gulp.watch('assets/**/*.scss', bundle_css);
});
