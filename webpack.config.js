const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',

    entry: {
        main: [
            path.join(__dirname, './assets/scripts/main.js'),
            path.join(__dirname, './assets/styles/main.scss'),
        ]

    },
    output: {
        path: path.resolve(__dirname, 'dist/'),
    },

    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
        }),
    ],


};